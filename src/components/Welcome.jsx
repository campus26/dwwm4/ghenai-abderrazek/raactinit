import { AiFillCalendar } from "react-icons/ai";
//1er composant 
export function Welcome({props}) {
   
   // Obtenir la date actuelle
   const currentDate = new Date();
   const formattedDate = currentDate.toLocaleDateString('fr-FR', {
    
    year: 'numeric',
    month: 'short',
    day: 'numeric'
});
    return (
    <div className="header">
    <h1>Todo </h1>
    <div className="calendar">
        <div className="date">
    <h2>Today</h2>
    <p>{formattedDate}</p>
    </div>
    <div >
        <AiFillCalendar style={{color: 'rgb(76,83,241)', height: '3em', width: "3rem", padding: "10px"}} />

    </div>
    </div>
    </div>

    );

}