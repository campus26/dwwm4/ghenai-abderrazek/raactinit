import React, { useState } from 'react'; // Import des modules React et useState depuis la bibliothèque React

export function AddTask({ onAddTask }) { // Définition du composant fonctionnel AddTask et déstructuration de la prop onAddTask

    const [task, setTask] = useState([]); // Déclaration de l'état task et de la fonction setTask à l'aide du hook useState, initialisé à une chaîne vide

    const handleChange = (e) => { // Fonction pour gérer les changements dans le champ de saisie
        setTask(e.target.value); // Mettre à jour l'état task avec la valeur du champ de saisie
    };

    const handleSubmit = (e) => { // Fonction pour gérer la soumission du formulaire
        e.preventDefault(); // Empêcher le comportement par défaut du formulaire (rechargement de la page)
        if (task.trim() !== '') { // Vérifier si la tâche n'est pas vide avant d'appeler onAddTask trim permet de pas prendre en sidération l'aspace avant et après
            onAddTask(task); // Ajouter la nouvelle tâche en appelant la fonction onAddTask
            setTask('');
        }
    };
    

    return (
        <div className="form"> {/* Début de la section du formulaire avec une classe CSS "form" */}
            <form onSubmit={handleSubmit}> {/* Définition du formulaire avec la fonction handleSubmit comme gestionnaire d'événements onSubmit */}
                <input type="text" placeholder='Ajouter une tâche' value={task} onChange={handleChange} /> {/* Champ de saisie contrôlé avec la valeur de l'état task et la fonction handleChange comme gestionnaire d'événements onChange */}
                <button type="submit">+</button> {/* Bouton de soumission du formulaire */}
               
                
            </form>
        </div> //Fin de la section du formulaire 
        
    );
}
