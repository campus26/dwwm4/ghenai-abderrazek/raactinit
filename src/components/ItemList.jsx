import React, { useState } from 'react'; // Import des modules React et useState depuis la bibliothèque React
import { AddTask } from './AddTask'; // Import du composant AddTask depuis le fichier AddTask.jsx
import { nanoid } from 'nanoid'; // Import de la fonction nanoid depuis la bibliothèque nanoid

export function ItemList() {
    // Initialiser l'état tasks avec les données initiales du stockage local
    const [tasks, setTasks] = useState(() => {
        const storedTasks = JSON.parse(localStorage.getItem('tasks'));
        return storedTasks || [];// effectue une opération logique "OU" (||). Si storedTasks (les données récupérées du stockage local) est null ou undefined, l'expression renvoie un tableau vide ([]). Cela garantit qu'en cas d'absence de données dans le stockage local, useState initialise l'état tasks avec un tableau vide par défaut.

    });
  

    // Gérer l'ajout d'une nouvelle tâche à la liste
    const handleAddTask = (newTask) => {
        // Mettre à jour l'état tasks avec la nouvelle tâche
        const updatedTasks = [...tasks, { id: nanoid(), name: newTask }];
        setTasks(updatedTasks);

        // Sauvegarder les tâches dans le stockage local
        localStorage.setItem('tasks', JSON.stringify(updatedTasks));
    };
  
    // Supprimer une tâche en fonction de son identifiant
    const deleteTask = (id) => {
        const updatedTasks = tasks.filter(task => task.id !== id);
        setTasks(updatedTasks);
        localStorage.setItem('tasks', JSON.stringify(updatedTasks));
    };

    return (
        <div >
            <AddTask onAddTask={handleAddTask} />
            {tasks.map((task) => (
                <div className= "item" key={task.id} >
                    <label>
                        <input type="checkbox" />
                        {task.name} {/* Afficher le nom de la tâche */}
                        
                    </label>
                    <button onClick={() => deleteTask(task.id)}>X</button>
                </div>
            ))}
        </div>
    );
}
