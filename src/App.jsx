
import { Welcome } from "./components/Welcome";
import { ItemList} from "./components/ItemList";

import '/src/styles/app.scss'

function App() {
  

  return (
    <>
    <div className="container">
    <header>
    <Welcome />
    </header>
      <div>
    
        <ItemList />
        
   
      </div>
      </div>
    
    </>
  )
}

export default App
